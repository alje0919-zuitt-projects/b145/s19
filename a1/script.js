console.log('Hello world');

let num1 = 2;
const getCube = num1 ** 3;

// 4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is.

console.log(`The cube of ${num1} is ${getCube}.`);

// 5. Create a variable address with a value of an array containing details of an address.

const address = ["Hollywood Blvd.", "Los Angeles", "California", 90046];


const [street, city, state, zipCode] = address;

console.log(`I live in ${street} ${city}, ${state} ${zipCode}.`)

// 7. Create a variable animal with a value of an object data type with different animal details as it’s properties.

let animalName = 'Tasmanian devil';
const animal = {
	d1: "carnivorous",
	d2: "marsupial",
	d3: "Dasyuridae"
}

const {d1, d2, d3} = animal;

console.log(`The ${animalName} is a ${d1} ${d2} of the family ${d3}.`)

//  9. Create an array of numbers.
// Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.

const numbers = [1, 2, 3, 4, 5];

numbers.forEach(num => {
	console.log(num);
})

let reduceNumber = numbers.reduce((x, y) => x + y);
console.log(reduceNumber);


// 12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
// 13. Create/instantiate a new object from the class Dog and console log the object.

class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
 }

 const myDog = new Dog();

 myDog.name = "Frankie";
 myDog.age = 5;
 myDog.breed = "Miniature Dachshund";
 console.log(myDog);