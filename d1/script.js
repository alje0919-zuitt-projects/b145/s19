console.log('Hello world');

// ES6 Updates

// 1. Exponent Operator
// before
const firstNum = Math.pow(8, 2);

// after
const secondNum = 8 ** 2;

// 2. Template Literals
// allows to write strings without using the concatenation operator (+)
// uses backticks(``) ${expression}

const interestRate = .1;
const principal = 1000;

console.log(`The interest on your savings account is: ${principal * interestRate}`);

// 3. Array destructuring
// allows us to unpack elements in arrays into distinct variables
// syntax:
	// let const [(variableNameA, variableNameB, variableNameC] = array;

const fullName = ["Juan", "Dela", "Cruz"];

// Pre-array destructuting
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you.`)

// Array Destructuring
const [firstName, middleName, lastName] = fullName;

console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you.`)
// helps with code readability


// 4. Object Destructuring
// allows to unpack properties of objects into distinct variables
// syntax:
	// let const {propertyName, propertyName, propertyName} = person;

const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
}

// Pre-object Desctructuring
console.log(person.givenName)
console.log(person.maidenName)
console.log(person.familyName)

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again.`);

// Object Desctructuring

const {givenName, maidenName, familyName} = person;
console.log(givenName);
console.log(maidenName);
console.log(familyName);
// Shortens the syntax for accessing properties from objects

function getFullName({givenName, maidenName, familyName}) {
	console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you.`)
}

getFullName(person);


// 5. Arrow Function

// traditional functions

function printFullName(firstName, middleName, lastName) {
	console.log(`${firstName} ${middleName} ${lastName}`)
}

printFullName("John", "Dela", "Cruz");

// Arrow Function
// syntax:
	/*
	let/const variableName = (parameterA, parameterB) => {
		statement
	}
	*/

let printFullName1 = (firstName, middleInitial, lastName) => {
	console.log(`${firstName} ${middleName} ${lastName}`)
}

printFullName("Jane", "D", "Smith");

//another example ----------------------

const students = ["John", "Jane", "Jasmine"];

students.forEach(function(student){
	console.log(`${student} is a student.`)
})

// Arrow function
students.forEach(student => {
	console.log(`${student} is a student.`)
})
// we can omit the paranthesis in arrow function if we only have one parameter.

// Implicit Return Statement
// There are instances when you can omit the "return" statement

// Pre-arrow function
/*
const add = (x, y) => {
	return x + y
}

let total = add(1, 2);
console.log(total);
*/

// Arrow Function
const add = (x, y) => x + y;

let total = add(1, 2);
console.log(total);

// Default Function Argument Value
// provide a default argument value if non is provided when the function is invoked.

//const greet = (name) => { --- will make console undefined unless provide default value
const greet = (name = 'User') => {
	return `Good morning, ${name}!`
}
console.log(greet());


// Class-based Object Blueprints
// allows creation of objects using classes as blueprints
// creating a class
	// constructor is a special method of a class for creating an object
	// this keyword that referes to the properties of an object created from the class

/*
function objectName() {
	this.keyA = keyA
}
*/

class Car {
	constructor(brand, name, year) {
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
 }

 const myCar = new Car();

 console.log(myCar);

 myCar.brand = "Ford";
 myCar.name = "Ranger Raptor";
 myCar.year = 2021;
 console.log(myCar);

 const myNewCar = new Car("Toyota", "Vios", 2021);
 console.log(myNewCar);

 // Ternary Operator
 (condition) ? truthy : falsy